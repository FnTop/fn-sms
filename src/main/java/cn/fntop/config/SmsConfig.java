package cn.fntop.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class SmsConfig {
    private final OpenApiProperties properties;

    @Bean
    public Sms sms() {
        if (properties.getOpenGateway() == null || "http://127.0.0.1:8080/".equals(properties.getOpenGateway())) {
            properties.setOpenGateway("https://sms.idowe.com/api/sms/");
        }
        return (Sms) Sms.getInstance(properties, SmsApi.class, Sms.class);
    }
}