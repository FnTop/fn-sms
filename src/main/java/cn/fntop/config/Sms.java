package cn.fntop.config;

import cn.fntop.entity.SmsMessage;
import cn.fntop.entity.SmsRequest;
import cn.fntop.entity.SmsResponse;
import cn.fntop.service.OpenApiService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Sms extends OpenApiService {
    private SmsApi api;

    public Sms() {
        this.api = (SmsApi) OpenApiService.api;
    }

    public SmsMessage<SmsResponse> send(SmsRequest request) {
        return execute(api.send(request.sign()));
    }

}