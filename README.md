<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">LLSMS</h1>
<h4 align="center">来了短信</h4>
﻿<p align="center">
 <img src="https://img.shields.io/badge/version-v2.2.1-green" alt="Fn">
 <img src="https://img.shields.io/badge/Apache-v2.0-green" alt="Apache2.0"><br/>
 <img src="https://img.shields.io/badge/Maven-v3.5.2-blue" alt="Maven">
<img src="https://img.shields.io/badge/Java-v1.8-blue">
<img src="https://img.shields.io/badge/SpringBoot-v2.6.2-blue">
</p>

### 📒📒介绍

[官网](http://lltd.fntop.cn)

该项目是`来了短信`对接的一个`spring-boot-starter`, 通过引用依赖即可快速发送短信

#### 📜📜Maven

```xml

<dependency>
    <groupId>cn.fntop</groupId>
    <artifactId>llsms-spring-boot-starter</artifactId>
    <version>2.2.1</version>
</dependency>

```

### 🏷️🏷️使用方式

`注意：图片验证码需要自建平台搭建哦，且一定要图片验证码，否则平台会被模拟攻击，本平台短信接口是通过 签名+验签+接口限流 保证接口安全的。`

#### 方式一：Java直接调用，前端通过图片验证码或者其他交互式验证防止机器攻击（更便捷，安全系数良好）

- Java后端调用方式

```java
//1、自动注入该对象 ;
//或者private final Sms sms
@Autowired
private Sms sms;

//2、构建短信对象
Dict map=Dict.create();
map.put("code","1654");
SmsRequest smsRequest=SmsRequest.builder().sid("签名id")
.templateId("模板id").phone("手机号").appId("平台AppId")
.appSecret("平台秘钥").data(map).build();

//3、调用
sms.send(smsRequest);
```

#### 方式二：Java直接调用，但是前端传递timestamp（时间戳）+nonce（随机串）+sign（签名）+图片验证码 （更复杂，安全系数优良。推荐）

- Java调用方式

```java
//1、自动注入该对象 ;
//或者private final Sms sms
@Autowired
private Sms sms;

//2、构建短信对象
Dict map=Dict.create();
map.put("code","1654");
SmsRequest smsRequest=SmsRequest.builder().sid("平台申请的签名id")
.templateId("平台申请的模板id").phone("手机号").appId("平台申请的AppId").timestamp("前端传的时间戳")
.nonce("前端传的随机字符串").sign("前端传的签名").data(map).build()

//3、调用
sms.send(smsRequest);

```

- 前端签名生成

```js
//appId + secretKey 请放到前端的配置文件中。
export function genSign(data: any, secretKey: any) {
    const signstr = "appId=" + data.appId + "&phone=" + data.phone + "&timestamp=" + data.timestamp + "&nonce=" + data.nonce + "&secretKey=" + secretKey
    const md5: any = new Md5()
    md5.appendAsciiStr(signstr)
    return md5.end()
}

```

#### 方式三：js调用+图片验证码（更便捷，安全系数良好）

- js调用方式

```js
//1.调用接口地址：https://sms.idowe.com/api/sms/core/send
//2.请求方式：post
//3.传递参数：body
const data = {
        phone: phone,
        templateId: templateId,
        sid: sid,
        appId: appId,
        timestamp: timestamp,
        nonce: nonce,
        sign: genSign(data, secretKey),
        data: {
            code: 123456
        },
}

```

#### 方式四：自建加签+验签+图片验证码（更复杂更安全，处理速度稍微慢点）

### 📗📗注意事项

- 如果模板是 `【测试】你的验证码是：{code}`传递的data是 `map.put("code", "1654")`;而不是`map.put("{code}", "1654")`;

- 只有`通过官网注册`的用户才能使用该工具对接

- 签名`必须过审`才能发送短信，如果是`默认模板无需过审`，`自定义模板需要过审`才能发送短信

### 📄📄更新说明

#### 2024-01-10 <img src="https://img.shields.io/badge/version-2.2.1-green">

- web平台更新
- 引入依赖即可快速对接短信

### 联系方式

`wx:gensui_`


